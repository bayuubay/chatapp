const http = require("http");
const express = require("express");
const database = require("./config/db");
// const socketio = require("socket.io");
const cors = require("cors");

const router = require("./src/router");

const app = express();
app.use(cors());
app.use(express.json())
app.use(express.urlencoded({extended:true}))
const server = http.createServer(app);

// const io = socketio(server);
const io = require("socket.io")(server, {
  cors: {
    origin: "http://localhost:3000",
    methods: ["GET", "POST"],
    allowedHeaders: ["my-custom-header"],
    credentials: true
  }
});
database();

app.use(router);

io.on("connection", (socket) => {
  console.log("new Connection");

  socket.on('join',({name,room},callback)=>{
     console.log([name,room])

  })

  socket.on("disconnect", () => {
    console.log("user leave");
  });
});

server.listen(process.env.PORT || 8000, () =>
  console.log(`Server has started.`)
);
