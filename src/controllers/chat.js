const User = require("../models/users");
const Room = require("../models/rooms");
const Message = require("../models/messages");

class Chat {
  async join(req, res,next) {
    try {
      //input user and room
      const { username, roomId } = req.body;

      const userExist=await User.findOne({username})
      const roomExist=await Room.findOne({name:roomId})
      if(!userExist){
        await User.create({ username });
      }
      if(!roomExist){
        await Room.create({ name: roomId });
      }
      

      return res.json({
        message: "success",
        code: 200,
        result: {
          users: username,
          room: roomId,
        },
      });
    } catch (error) {
      console.log(error);
    }
  }

  async chatRoom(req,res){
      const {username,room}=req.query
  }

  async newMessage(req, res) {
    try {
      const { username, roomId } = req.query;
      const { message } = req.body;
      const newMessage = await Message.create({
        username: username,
        room: roomId,
        message,
      });

      return res.json({
          messages:'success',
          newMessage

      })
    } catch (error) {
        console.log(error)
    }
  }
}

module.exports = Object.freeze(new Chat());
