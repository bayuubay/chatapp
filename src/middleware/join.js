const User = require("../models/users");
const Room = require("../models/rooms");

module.exports = async (req, res, next) => {
  try {
    const { username, roomId } = req.body;
    const userExist = await User.findOne({ username });
    const roomExist = await Room.findOne({ name: roomId });

    if (userExist || roomExist) {
        throw new Error("user or room already existed");
    }

    next();
  } catch (error) {
    console.log(error);
    res.send(error.message)
  }
};
