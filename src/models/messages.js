const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const ChatSchema = new Schema({
  username: {
    type: mongoose.Schema.Types.ObjectId,
    ref:"users",
    required: true,
  },
  room: {
    type: mongoose.Schema.Types.ObjectId,
    ref:"rooms",
    required: true,
  },
  message: {
    type: String,
    default: null,
  },
});

module.exports = mongoose.model("chats", ChatSchema);

