const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const roomSchema = new Schema({
  name: {
    type: String,
    required: false,
  },
});

module.exports = mongoose.model("rooms", roomSchema);

